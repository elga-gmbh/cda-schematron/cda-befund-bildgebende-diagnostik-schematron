<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.13.3.18
Name: Criticality Observation
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.13.3.18-2017-08-10T195216">
   <title>Criticality Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]"
         id="d42e922-false-d4129e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]) &gt;= 1">(CriticalityObservation): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]) &lt;= 1">(CriticalityObservation): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]"
         id="d42e945-false-d4146e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="string(@classCode) = ('OBS')">(CriticalityObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="string(@moodCode) = ('EVN')">(CriticalityObservation): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']) &gt;= 1">(CriticalityObservation): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.18'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']) &lt;= 1">(CriticalityObservation): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.18'] kommt zu häufig vor [max 1x].</assert>
      <report fpi="CD-UNKN-BSP"
              role="warning"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:code[(@code = '82606-5' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt; 1">(CriticalityObservation): Element hl7:code ist codiert mit Bindungsstärke 'CWE' und enthält ein Code außerhalb des angegebene Wertraums.</report>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:code[(@code = '82606-5' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(CriticalityObservation): Element hl7:code[(@code = '82606-5' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:text) &lt;= 1">(CriticalityObservation): Element hl7:text kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(CriticalityObservation): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(CriticalityObservation): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(CriticalityObservation): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(CriticalityObservation): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']"
         id="d42e951-false-d4207e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(CriticalityObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="string(@root) = ('1.2.40.0.34.11.13.3.18')">(CriticalityObservation): Der Wert von root MUSS '1.2.40.0.34.11.13.3.18' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:code[(@code = '82606-5' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:code[(@code = '82606-5' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e956-false-d4222e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(CriticalityObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="@nullFlavor or (@code='82606-5' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Criticality' and @codeSystemName='LOINC')">(CriticalityObservation): Der Elementinhalt MUSS einer von 'code '82606-5' codeSystem '2.16.840.1.113883.6.1' displayName='Criticality' codeSystemName='LOINC'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:text
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:text"
         id="d42e961-false-d4238e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(CriticalityObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(CriticalityObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d42e966-false-d4254e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(CriticalityObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="@value">(CriticalityObservation): Attribut @value MUSS vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:statusCode[@code = 'completed']
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:statusCode[@code = 'completed']"
         id="d42e977-false-d4269e0">
      <extends rule="CS"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(CriticalityObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="@nullFlavor or (@code='completed')">(CriticalityObservation): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.18
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (CriticalityObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.18']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e982-false-d4288e0">
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.182-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(CriticalityObservation): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.182 ELGA_CriticalityObservationValue (DYNAMIC)' sein.</assert>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.18-2017-08-10T195216.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3') or not(@xsi:type)">(CriticalityObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
