<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.13.3.19
Name: Certainty Observation
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.13.3.19-2017-08-10T202554">
   <title>Certainty Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]"
         id="d42e994-false-d4317e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]) &gt;= 1">(CertaintyObservation): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]) &lt;= 1">(CertaintyObservation): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]"
         id="d42e1029-false-d4334e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="string(@classCode) = ('OBS')">(CertaintyObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="string(@moodCode) = ('EVN')">(CertaintyObservation): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']) &gt;= 1">(CertaintyObservation): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.19'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']) &lt;= 1">(CertaintyObservation): Element hl7:templateId[@root = '1.2.40.0.34.11.13.3.19'] kommt zu häufig vor [max 1x].</assert>
      <report fpi="CD-UNKN-BSP"
              role="warning"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:code[(@code = '66455-7' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt; 1">(CertaintyObservation): Element hl7:code ist codiert mit Bindungsstärke 'extensible' und enthält ein Code außerhalb des angegebene Wertraums.</report>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:code[(@code = '66455-7' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(CertaintyObservation): Element hl7:code[(@code = '66455-7' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:text) &lt;= 1">(CertaintyObservation): Element hl7:text kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(CertaintyObservation): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(CertaintyObservation): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &gt;= 1">(CertaintyObservation): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]) &lt;= 1">(CertaintyObservation): Element hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']"
         id="d42e1035-false-d4395e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(CertaintyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="string(@root) = ('1.2.40.0.34.11.13.3.19')">(CertaintyObservation): Der Wert von root MUSS '1.2.40.0.34.11.13.3.19' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:code[(@code = '66455-7' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:code[(@code = '66455-7' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e1040-false-d4410e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(CertaintyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="@nullFlavor or (@code='66455-7' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Condition status' and @codeSystemName='LOINC')">(CertaintyObservation): Der Elementinhalt MUSS einer von 'code '66455-7' codeSystem '2.16.840.1.113883.6.1' displayName='Condition status' codeSystemName='LOINC'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:text
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:text"
         id="d42e1048-false-d4426e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(CertaintyObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(CertaintyObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d42e1053-false-d4442e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(CertaintyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="@value">(CertaintyObservation): Attribut @value MUSS vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:statusCode[@code = 'completed']
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:statusCode[@code = 'completed']"
         id="d42e1064-false-d4457e0">
      <extends rule="CS"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(CertaintyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="@nullFlavor or (@code='completed')">(CertaintyObservation): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.19
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]
Item: (CertaintyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.13.3.19']]/hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]"
         id="d42e1069-false-d4476e0">
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.184-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(CertaintyObservation): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.184 ELGA_ConditionVerificationStatus (DYNAMIC)' sein.</assert>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.11.13.3.19-2017-08-10T202554.html"
              test="@nullFlavor or ($xsiLocalName='CD' and $xsiLocalNS='urn:hl7-org:v3') or not(@xsi:type)">(CertaintyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
