<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.43
Name: Geplante Untersuchungen
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640-closed">
   <title>Geplante Untersuchungen</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']])]"
         id="d42e20952-true-d1016808e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d42e20952-true-d1016808e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']] (rule-reference: d42e20952-true-d1016808e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '59772-4' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e20985-true-d1017292e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d42e20985-true-d1017292e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '59772-4' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e20985-true-d1017292e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e21016-true-d1017444e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d42e21016-true-d1017444e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e21016-true-d1017444e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1017322e45-true-d1017552e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017322e45-true-d1017552e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1017322e45-true-d1017552e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1017322e64-true-d1017611e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017322e64-true-d1017611e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1017322e64-true-d1017611e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1017322e110-true-d1017668e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017322e110-true-d1017668e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1017322e110-true-d1017668e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1017672e92-true-d1017702e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1017672e92-true-d1017702e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1017672e92-true-d1017702e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1017322e133-true-d1017750e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017322e133-true-d1017750e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1017322e133-true-d1017750e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1017322e149-true-d1017795e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017322e149-true-d1017795e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1017322e149-true-d1017795e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1017765e67-true-d1017858e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017765e67-true-d1017858e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1017765e67-true-d1017858e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e21022-true-d1018026e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d42e21022-true-d1018026e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e21022-true-d1018026e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1017903e15-true-d1018158e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017903e15-true-d1018158e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1017903e15-true-d1018158e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1018031e50-true-d1018224e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018031e50-true-d1018224e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1018031e50-true-d1018224e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1018031e120-true-d1018286e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018031e120-true-d1018286e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1018031e120-true-d1018286e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1018031e132-true-d1018308e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018031e132-true-d1018308e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1018031e132-true-d1018308e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1018296e12-true-d1018337e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018296e12-true-d1018337e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1018296e12-true-d1018337e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1018031e143-true-d1018388e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018031e143-true-d1018388e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1018031e143-true-d1018388e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1018362e58-true-d1018449e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018362e58-true-d1018449e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1018362e58-true-d1018449e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1017903e17-true-d1018526e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017903e17-true-d1018526e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1017903e17-true-d1018526e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1017903e26-true-d1018579e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017903e26-true-d1018579e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1017903e26-true-d1018579e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1017903e30-true-d1018634e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1017903e30-true-d1018634e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1017903e30-true-d1018634e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1018627e10-true-d1018661e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018627e10-true-d1018661e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1018627e10-true-d1018661e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e21034-true-d1018971e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d42e21034-true-d1018971e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e21034-true-d1018971e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d1018692e8-true-d1019284e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018692e8-true-d1019284e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d1018692e8-true-d1019284e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1018692e59-true-d1019438e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018692e59-true-d1019438e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1018692e59-true-d1019438e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1019316e45-true-d1019546e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019316e45-true-d1019546e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1019316e45-true-d1019546e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1019316e64-true-d1019605e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019316e64-true-d1019605e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1019316e64-true-d1019605e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1019316e110-true-d1019662e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019316e110-true-d1019662e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1019316e110-true-d1019662e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1019666e92-true-d1019696e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1019666e92-true-d1019696e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1019666e92-true-d1019696e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1019316e133-true-d1019744e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019316e133-true-d1019744e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1019316e133-true-d1019744e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1019316e149-true-d1019789e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019316e149-true-d1019789e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1019316e149-true-d1019789e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1019759e67-true-d1019852e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019759e67-true-d1019852e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1019759e67-true-d1019852e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1018692e65-true-d1020020e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1018692e65-true-d1020020e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1018692e65-true-d1020020e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1019897e5-true-d1020152e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019897e5-true-d1020152e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1019897e5-true-d1020152e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1020025e50-true-d1020218e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1020025e50-true-d1020218e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1020025e50-true-d1020218e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1020025e120-true-d1020280e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1020025e120-true-d1020280e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1020025e120-true-d1020280e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1020025e132-true-d1020302e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1020025e132-true-d1020302e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1020025e132-true-d1020302e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1020290e12-true-d1020331e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1020290e12-true-d1020331e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1020290e12-true-d1020331e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1020025e143-true-d1020382e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1020025e143-true-d1020382e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1020025e143-true-d1020382e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1020356e58-true-d1020443e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1020356e58-true-d1020443e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1020356e58-true-d1020443e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1019897e7-true-d1020520e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019897e7-true-d1020520e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1019897e7-true-d1020520e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1019897e16-true-d1020573e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019897e16-true-d1020573e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1019897e16-true-d1020573e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1019897e20-true-d1020628e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1019897e20-true-d1020628e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1019897e20-true-d1020628e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.43']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1020621e10-true-d1020655e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.html"
              test="not(.)">(elgagab_section_geplanteUntersuchungen)/d1020621e10-true-d1020655e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1020621e10-true-d1020655e0)</assert>
   </rule>
</pattern>
