<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.39
Name: Medical Device Entry
Description: Dieses Entry beschreibt das medizinisches Gerät oder Implantat, das vom Patienten zur Zeit verwendet wird.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204-closed">
   <title>Medical Device Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/*[not(@xsi:nil = 'true')][not(self::hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']])]"
         id="d42e44974-true-d1941216e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e44974-true-d1941216e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']] (rule-reference: d42e44974-true-d1941216e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | self::hl7:id | self::hl7:text | self::hl7:effectiveTime | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e45044-true-d1941555e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45044-true-d1941555e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | hl7:id | hl7:text | hl7:effectiveTime | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e45044-true-d1941555e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e45067-true-d1941586e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45067-true-d1941586e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e45067-true-d1941586e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low)]"
         id="d42e45082-true-d1941605e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45082-true-d1941605e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low (rule-reference: d42e45082-true-d1941605e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e45104-true-d1941737e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45104-true-d1941737e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e45104-true-d1941737e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1941615e46-true-d1941845e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1941615e46-true-d1941845e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1941615e46-true-d1941845e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1941615e65-true-d1941904e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1941615e65-true-d1941904e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1941615e65-true-d1941904e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1941615e111-true-d1941961e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1941615e111-true-d1941961e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1941615e111-true-d1941961e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1941965e92-true-d1941995e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1941965e92-true-d1941995e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1941965e92-true-d1941995e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1941615e134-true-d1942043e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1941615e134-true-d1942043e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1941615e134-true-d1942043e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1941615e150-true-d1942088e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1941615e150-true-d1942088e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1941615e150-true-d1942088e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1942058e67-true-d1942151e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942058e67-true-d1942151e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1942058e67-true-d1942151e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e45111-true-d1942319e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45111-true-d1942319e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e45111-true-d1942319e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1942196e15-true-d1942451e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942196e15-true-d1942451e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1942196e15-true-d1942451e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1942324e50-true-d1942517e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942324e50-true-d1942517e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1942324e50-true-d1942517e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1942324e120-true-d1942579e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942324e120-true-d1942579e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1942324e120-true-d1942579e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1942324e132-true-d1942601e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942324e132-true-d1942601e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1942324e132-true-d1942601e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1942589e12-true-d1942630e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942589e12-true-d1942630e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1942589e12-true-d1942630e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1942324e143-true-d1942681e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942324e143-true-d1942681e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1942324e143-true-d1942681e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1942655e58-true-d1942742e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942655e58-true-d1942742e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1942655e58-true-d1942742e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1942196e17-true-d1942819e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942196e17-true-d1942819e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1942196e17-true-d1942819e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1942196e26-true-d1942872e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942196e26-true-d1942872e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1942196e26-true-d1942872e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1942196e30-true-d1942927e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942196e30-true-d1942927e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1942196e30-true-d1942927e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1942920e10-true-d1942954e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1942920e10-true-d1942954e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1942920e10-true-d1942954e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/*[not(@xsi:nil = 'true')][not(self::hl7:participantRole)]"
         id="d42e45123-true-d1943004e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45123-true-d1943004e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:participantRole (rule-reference: d42e45123-true-d1943004e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:playingDevice)]"
         id="d42e45130-true-d1943038e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45130-true-d1943038e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:playingDevice (rule-reference: d42e45130-true-d1943038e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code)]"
         id="d42e45146-true-d1943072e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45146-true-d1943072e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code (rule-reference: d42e45146-true-d1943072e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d42e45152-true-d1943096e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45152-true-d1943096e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d42e45152-true-d1943096e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference)]"
         id="d42e45252-true-d1943110e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45252-true-d1943110e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference (rule-reference: d42e45252-true-d1943110e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e45277-true-d1943148e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45277-true-d1943148e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e45277-true-d1943148e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1943125e5-true-d1943181e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1943125e5-true-d1943181e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1943125e5-true-d1943181e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1943201e54-true-d1943213e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1943201e54-true-d1943213e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1943201e54-true-d1943213e0)</assert>
   </rule>
</pattern>
