<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.39
Name: Medical Device Entry
Description: Dieses Entry beschreibt das medizinisches Gerät oder Implantat, das vom Patienten zur Zeit verwendet wird.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826-closed">
   <title>Medical Device Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/*[not(@xsi:nil = 'true')][not(self::hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']])]"
         id="d42e45285-true-d1946936e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45285-true-d1946936e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']] (rule-reference: d42e45285-true-d1946936e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | self::hl7:id | self::hl7:text | self::hl7:effectiveTime | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e45353-true-d1947275e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45353-true-d1947275e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | hl7:id | hl7:text | hl7:effectiveTime | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e45353-true-d1947275e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e45376-true-d1947306e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45376-true-d1947306e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e45376-true-d1947306e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low)]"
         id="d42e45391-true-d1947325e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45391-true-d1947325e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low (rule-reference: d42e45391-true-d1947325e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e45413-true-d1947457e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45413-true-d1947457e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e45413-true-d1947457e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1947335e46-true-d1947565e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947335e46-true-d1947565e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1947335e46-true-d1947565e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1947335e65-true-d1947624e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947335e65-true-d1947624e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1947335e65-true-d1947624e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1947335e111-true-d1947681e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947335e111-true-d1947681e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1947335e111-true-d1947681e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1947685e92-true-d1947715e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1947685e92-true-d1947715e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1947685e92-true-d1947715e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1947335e134-true-d1947763e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947335e134-true-d1947763e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1947335e134-true-d1947763e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1947335e150-true-d1947808e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947335e150-true-d1947808e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1947335e150-true-d1947808e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1947778e67-true-d1947871e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947778e67-true-d1947871e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1947778e67-true-d1947871e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e45420-true-d1948039e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45420-true-d1948039e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e45420-true-d1948039e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1947916e15-true-d1948171e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947916e15-true-d1948171e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1947916e15-true-d1948171e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1948044e50-true-d1948237e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948044e50-true-d1948237e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1948044e50-true-d1948237e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1948044e120-true-d1948299e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948044e120-true-d1948299e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1948044e120-true-d1948299e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1948044e132-true-d1948321e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948044e132-true-d1948321e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1948044e132-true-d1948321e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1948309e12-true-d1948350e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948309e12-true-d1948350e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1948309e12-true-d1948350e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1948044e143-true-d1948401e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948044e143-true-d1948401e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1948044e143-true-d1948401e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1948375e58-true-d1948462e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948375e58-true-d1948462e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1948375e58-true-d1948462e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1947916e17-true-d1948539e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947916e17-true-d1948539e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1947916e17-true-d1948539e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1947916e26-true-d1948592e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947916e26-true-d1948592e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1947916e26-true-d1948592e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1947916e30-true-d1948647e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1947916e30-true-d1948647e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1947916e30-true-d1948647e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1948640e10-true-d1948674e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948640e10-true-d1948674e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1948640e10-true-d1948674e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/*[not(@xsi:nil = 'true')][not(self::hl7:participantRole)]"
         id="d42e45432-true-d1948724e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45432-true-d1948724e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:participantRole (rule-reference: d42e45432-true-d1948724e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:playingDevice)]"
         id="d42e45439-true-d1948758e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45439-true-d1948758e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:playingDevice (rule-reference: d42e45439-true-d1948758e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code)]"
         id="d42e45455-true-d1948792e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45455-true-d1948792e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code (rule-reference: d42e45455-true-d1948792e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d42e45461-true-d1948816e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45461-true-d1948816e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d42e45461-true-d1948816e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference)]"
         id="d42e45561-true-d1948830e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45561-true-d1948830e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference (rule-reference: d42e45561-true-d1948830e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e45586-true-d1948868e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d42e45586-true-d1948868e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e45586-true-d1948868e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1948845e5-true-d1948901e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.39-2019-11-21T130826.html"
              test="not(.)">(atcdabbr_entry_MedicalDevice)/d1948845e5-true-d1948901e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1948845e5-true-d1948901e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1948921e54-true-d1948933e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1948921e54-true-d1948933e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1948921e54-true-d1948933e0)</assert>
   </rule>
</pattern>
