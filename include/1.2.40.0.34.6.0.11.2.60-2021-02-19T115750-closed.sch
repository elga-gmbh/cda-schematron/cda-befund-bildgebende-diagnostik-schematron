<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.60
Name: Medizinische Geräte und Implantate - kodiert
Description: 
                 Diese Sektion enthält Informationen über intra- und extrakorporale Medizinprodukte oder Medizingeräte, von denen der Gesundheitszustand des Patienten direkt abhängig ist. Das umfasst z.B. Implantate, Prothesen, Pumpen, Herzschrittmacher etc. von denen ein GDA Kenntnis haben soll. 
                 Heilbehelfe wie Gehhilfen, Rollstuhl etc sind nicht notwendigerweise anzuführen. 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750-closed">
   <title>Medizinische Geräte und Implantate - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']])]"
         id="d42e24656-true-d1279767e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e24656-true-d1279767e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']] (rule-reference: d42e24656-true-d1279767e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e24861-true-d1280448e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e24861-true-d1280448e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e24861-true-d1280448e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e24905-true-d1280606e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e24905-true-d1280606e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e24905-true-d1280606e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1280484e46-true-d1280714e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1280484e46-true-d1280714e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1280484e46-true-d1280714e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1280484e65-true-d1280773e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1280484e65-true-d1280773e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1280484e65-true-d1280773e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1280484e111-true-d1280830e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1280484e111-true-d1280830e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1280484e111-true-d1280830e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1280834e92-true-d1280864e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1280834e92-true-d1280864e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1280834e92-true-d1280864e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1280484e134-true-d1280912e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1280484e134-true-d1280912e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1280484e134-true-d1280912e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1280484e150-true-d1280957e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1280484e150-true-d1280957e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1280484e150-true-d1280957e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1280927e67-true-d1281020e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1280927e67-true-d1281020e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1280927e67-true-d1281020e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e24912-true-d1281188e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e24912-true-d1281188e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e24912-true-d1281188e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1281065e15-true-d1281320e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281065e15-true-d1281320e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1281065e15-true-d1281320e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1281193e50-true-d1281386e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281193e50-true-d1281386e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1281193e50-true-d1281386e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1281193e120-true-d1281448e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281193e120-true-d1281448e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1281193e120-true-d1281448e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1281193e132-true-d1281470e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281193e132-true-d1281470e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1281193e132-true-d1281470e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1281458e12-true-d1281499e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281458e12-true-d1281499e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1281458e12-true-d1281499e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1281193e143-true-d1281550e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281193e143-true-d1281550e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1281193e143-true-d1281550e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1281524e58-true-d1281611e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281524e58-true-d1281611e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1281524e58-true-d1281611e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1281065e17-true-d1281688e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281065e17-true-d1281688e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1281065e17-true-d1281688e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1281065e26-true-d1281741e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281065e26-true-d1281741e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1281065e26-true-d1281741e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1281065e30-true-d1281796e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281065e30-true-d1281796e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1281065e30-true-d1281796e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1281789e10-true-d1281823e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281789e10-true-d1281823e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1281789e10-true-d1281823e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/*[not(@xsi:nil = 'true')][not(self::hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']])]"
         id="d42e24924-true-d1282150e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e24924-true-d1282150e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']] (rule-reference: d42e24924-true-d1282150e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | self::hl7:id | self::hl7:text | self::hl7:effectiveTime | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1281854e9-true-d1282489e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e9-true-d1282489e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26'] | hl7:id | hl7:text | hl7:effectiveTime | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']] | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1281854e9-true-d1282489e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1281854e32-true-d1282520e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e32-true-d1282520e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1281854e32-true-d1282520e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low)]"
         id="d1281854e47-true-d1282539e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e47-true-d1282539e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low (rule-reference: d1281854e47-true-d1282539e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1281854e69-true-d1282671e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e69-true-d1282671e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1281854e69-true-d1282671e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1282549e46-true-d1282779e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1282549e46-true-d1282779e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1282549e46-true-d1282779e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1282549e65-true-d1282838e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1282549e65-true-d1282838e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1282549e65-true-d1282838e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1282549e111-true-d1282895e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1282549e111-true-d1282895e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1282549e111-true-d1282895e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1282899e92-true-d1282929e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1282899e92-true-d1282929e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1282899e92-true-d1282929e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1282549e134-true-d1282977e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1282549e134-true-d1282977e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1282549e134-true-d1282977e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1282549e150-true-d1283022e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1282549e150-true-d1283022e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1282549e150-true-d1283022e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1282992e67-true-d1283085e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1282992e67-true-d1283085e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1282992e67-true-d1283085e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1281854e76-true-d1283253e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e76-true-d1283253e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1281854e76-true-d1283253e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1283130e15-true-d1283385e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283130e15-true-d1283385e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1283130e15-true-d1283385e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1283258e50-true-d1283451e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283258e50-true-d1283451e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1283258e50-true-d1283451e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1283258e120-true-d1283513e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283258e120-true-d1283513e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1283258e120-true-d1283513e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1283258e132-true-d1283535e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283258e132-true-d1283535e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1283258e132-true-d1283535e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1283523e12-true-d1283564e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283523e12-true-d1283564e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1283523e12-true-d1283564e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1283258e143-true-d1283615e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283258e143-true-d1283615e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1283258e143-true-d1283615e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1283589e58-true-d1283676e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283589e58-true-d1283676e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1283589e58-true-d1283676e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1283130e17-true-d1283753e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283130e17-true-d1283753e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1283130e17-true-d1283753e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1283130e26-true-d1283806e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283130e26-true-d1283806e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1283130e26-true-d1283806e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1283130e30-true-d1283861e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283130e30-true-d1283861e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1283130e30-true-d1283861e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1283854e10-true-d1283888e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1283854e10-true-d1283888e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1283854e10-true-d1283888e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/*[not(@xsi:nil = 'true')][not(self::hl7:participantRole)]"
         id="d1281854e88-true-d1283938e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e88-true-d1283938e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:participantRole (rule-reference: d1281854e88-true-d1283938e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:playingDevice)]"
         id="d1281854e95-true-d1283972e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e95-true-d1283972e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:playingDevice (rule-reference: d1281854e95-true-d1283972e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code)]"
         id="d1281854e111-true-d1284006e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e111-true-d1284006e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code (rule-reference: d1281854e111-true-d1284006e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation)]"
         id="d1281854e117-true-d1284030e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e117-true-d1284030e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation (rule-reference: d1281854e117-true-d1284030e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:participant[not(@nullFlavor)][@typeCode = 'DEV'][hl7:participantRole[@classCode = 'MANU']]/hl7:participantRole/hl7:playingDevice/hl7:code/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference)]"
         id="d1281854e217-true-d1284044e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e217-true-d1284044e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference (rule-reference: d1281854e217-true-d1284044e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1281854e242-true-d1284082e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1281854e242-true-d1284082e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1281854e242-true-d1284082e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1284059e5-true-d1284115e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284059e5-true-d1284115e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1284059e5-true-d1284115e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]]/hl7:supply[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.39'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.26']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1284135e54-true-d1284147e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1284135e54-true-d1284147e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1284135e54-true-d1284147e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e24937-true-d1284442e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d42e24937-true-d1284442e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e24937-true-d1284442e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d1284163e8-true-d1284755e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284163e8-true-d1284755e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d1284163e8-true-d1284755e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1284163e59-true-d1284909e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284163e59-true-d1284909e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1284163e59-true-d1284909e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1284787e45-true-d1285017e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284787e45-true-d1285017e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1284787e45-true-d1285017e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1284787e64-true-d1285076e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284787e64-true-d1285076e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1284787e64-true-d1285076e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1284787e110-true-d1285133e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284787e110-true-d1285133e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1284787e110-true-d1285133e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1285137e92-true-d1285167e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1285137e92-true-d1285167e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1285137e92-true-d1285167e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1284787e133-true-d1285215e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284787e133-true-d1285215e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1284787e133-true-d1285215e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1284787e149-true-d1285260e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284787e149-true-d1285260e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1284787e149-true-d1285260e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1285230e67-true-d1285323e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285230e67-true-d1285323e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1285230e67-true-d1285323e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1284163e65-true-d1285491e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1284163e65-true-d1285491e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1284163e65-true-d1285491e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1285368e5-true-d1285623e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285368e5-true-d1285623e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1285368e5-true-d1285623e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1285496e50-true-d1285689e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285496e50-true-d1285689e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1285496e50-true-d1285689e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1285496e120-true-d1285751e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285496e120-true-d1285751e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1285496e120-true-d1285751e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1285496e132-true-d1285773e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285496e132-true-d1285773e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1285496e132-true-d1285773e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1285761e12-true-d1285802e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285761e12-true-d1285802e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1285761e12-true-d1285802e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1285496e143-true-d1285853e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285496e143-true-d1285853e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1285496e143-true-d1285853e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1285827e58-true-d1285914e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285827e58-true-d1285914e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1285827e58-true-d1285914e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1285368e7-true-d1285991e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285368e7-true-d1285991e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1285368e7-true-d1285991e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1285368e16-true-d1286044e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285368e16-true-d1286044e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1285368e16-true-d1286044e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1285368e20-true-d1286099e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1285368e20-true-d1286099e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1285368e20-true-d1286099e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.60'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.3.6']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1286092e10-true-d1286126e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.html"
              test="not(.)">(atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert)/d1286092e10-true-d1286126e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1286092e10-true-d1286126e0)</assert>
   </rule>
</pattern>
