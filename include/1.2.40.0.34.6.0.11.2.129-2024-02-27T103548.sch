<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.129
Name: Schlussfolgerung
Description: 
                 
 In dieser Sektion beschreibt der Befundende seine Schlüsse aus den Vorstudien und dem aktuellen Bildmaterial. Keinesfalls sind in dieser Sektion daraus abgeleitete Empfehlungen zu formulieren, diese gehören in ihre eigene Sektion. 
 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548">
   <title>Schlussfolgerung</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.129
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]
Item: (elgabgd_section_Schlussfolgerung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.129
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]
Item: (elgabgd_section_Schlussfolgerung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]"
         id="d42e18219-false-d749091e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(elgabgd_section_Schlussfolgerung): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']) &gt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']) &lt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:code[(@code = '55110-1' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:code[(@code = '55110-1' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:code[(@code = '55110-1' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:code[(@code = '55110-1' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(elgabgd_section_Schlussfolgerung): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.129
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']
Item: (elgabgd_section_Schlussfolgerung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']"
         id="d42e18223-false-d749235e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(elgabgd_section_Schlussfolgerung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.129')">(elgabgd_section_Schlussfolgerung): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.129' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.129
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:code[(@code = '55110-1' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (elgabgd_section_Schlussfolgerung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:code[(@code = '55110-1' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e18228-false-d749250e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(elgabgd_section_Schlussfolgerung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="@nullFlavor or (@code='55110-1' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Conclusions' and @codeSystemName='LOINC')">(elgabgd_section_Schlussfolgerung): Der Elementinhalt MUSS einer von 'code '55110-1' codeSystem '2.16.840.1.113883.6.1' displayName='Conclusions' codeSystemName='LOINC'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.129
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:title[not(@nullFlavor)]
Item: (elgabgd_section_Schlussfolgerung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:title[not(@nullFlavor)]"
         id="d42e18233-false-d749266e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(elgabgd_section_Schlussfolgerung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="text()='Schlussfolgerung'">(elgabgd_section_Schlussfolgerung): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Schlussfolgerung'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.129
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:text[not(@nullFlavor)]
Item: (elgabgd_section_Schlussfolgerung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:text[not(@nullFlavor)]"
         id="d42e18239-false-d749280e0">
      <extends rule="ED"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.129-2024-02-27T103548.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(elgabgd_section_Schlussfolgerung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.129
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.129']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.19']]]
Item: (elgabgd_section_Schlussfolgerung)
--></pattern>
