<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.55
Name: Lebensstil - kodiert
Description: Diese Sektion dient der
                    Erfassung von Lebensstil-Faktoren, wie Alkoholkonsum oder Rauchen.  Bei der Angabe von Werten, auf einen Abusus hindeuten, soll auf Konsistenz mit den Angaben in den Diagnosen geachtet werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710-closed">
   <title>Lebensstil - kodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']])]"
         id="d42e24235-true-d1242729e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24235-true-d1242729e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']] (rule-reference: d42e24235-true-d1242729e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] | self::hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '29762-2' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]] | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]] | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]] | self::hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]] | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e24303-true-d1243969e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24303-true-d1243969e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] | hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '29762-2' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]] | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]] | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]] | hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]] | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e24303-true-d1243969e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e24349-true-d1244127e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24349-true-d1244127e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e24349-true-d1244127e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1244005e46-true-d1244235e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244005e46-true-d1244235e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1244005e46-true-d1244235e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1244005e65-true-d1244294e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244005e65-true-d1244294e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1244005e65-true-d1244294e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1244005e111-true-d1244351e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244005e111-true-d1244351e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1244005e111-true-d1244351e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1244355e92-true-d1244385e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1244355e92-true-d1244385e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1244355e92-true-d1244385e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1244005e134-true-d1244433e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244005e134-true-d1244433e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1244005e134-true-d1244433e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1244005e150-true-d1244478e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244005e150-true-d1244478e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1244005e150-true-d1244478e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1244448e67-true-d1244541e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244448e67-true-d1244541e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1244448e67-true-d1244541e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e24356-true-d1244709e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24356-true-d1244709e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e24356-true-d1244709e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1244586e15-true-d1244841e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244586e15-true-d1244841e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1244586e15-true-d1244841e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1244714e50-true-d1244907e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244714e50-true-d1244907e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1244714e50-true-d1244907e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1244714e120-true-d1244969e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244714e120-true-d1244969e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1244714e120-true-d1244969e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1244714e132-true-d1244991e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244714e132-true-d1244991e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1244714e132-true-d1244991e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1244979e12-true-d1245020e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244979e12-true-d1245020e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1244979e12-true-d1245020e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1244714e143-true-d1245071e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244714e143-true-d1245071e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1244714e143-true-d1245071e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1245045e58-true-d1245132e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1245045e58-true-d1245132e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1245045e58-true-d1245132e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1244586e17-true-d1245209e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244586e17-true-d1245209e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1244586e17-true-d1245209e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1244586e26-true-d1245262e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244586e26-true-d1245262e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1244586e26-true-d1245262e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1244586e30-true-d1245317e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1244586e30-true-d1245317e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1244586e30-true-d1245317e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1245310e10-true-d1245344e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1245310e10-true-d1245344e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1245310e10-true-d1245344e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']])]"
         id="d42e24368-true-d1245668e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24368-true-d1245668e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']] (rule-reference: d42e24368-true-d1245668e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41'] | self::hl7:id | self::hl7:code[(@code = '230056004' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.204-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1245375e12-true-d1246014e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1245375e12-true-d1246014e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41'] | hl7:id | hl7:code[(@code = '230056004' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.204-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1245375e12-true-d1246014e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1245375e27-true-d1246056e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1245375e27-true-d1246056e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1245375e27-true-d1246056e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1245375e53-true-d1246201e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1245375e53-true-d1246201e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1245375e53-true-d1246201e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1246079e43-true-d1246309e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246079e43-true-d1246309e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1246079e43-true-d1246309e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1246079e62-true-d1246368e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246079e62-true-d1246368e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1246079e62-true-d1246368e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1246079e108-true-d1246425e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246079e108-true-d1246425e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1246079e108-true-d1246425e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1246429e92-true-d1246459e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1246429e92-true-d1246459e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1246429e92-true-d1246459e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1246079e131-true-d1246507e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246079e131-true-d1246507e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1246079e131-true-d1246507e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1246079e147-true-d1246552e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246079e147-true-d1246552e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1246079e147-true-d1246552e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1246522e67-true-d1246615e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246522e67-true-d1246615e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1246522e67-true-d1246615e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1245375e56-true-d1246783e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1245375e56-true-d1246783e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1245375e56-true-d1246783e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1246660e12-true-d1246915e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246660e12-true-d1246915e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1246660e12-true-d1246915e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1246788e50-true-d1246981e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246788e50-true-d1246981e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1246788e50-true-d1246981e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1246788e120-true-d1247043e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246788e120-true-d1247043e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1246788e120-true-d1247043e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1246788e132-true-d1247065e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246788e132-true-d1247065e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1246788e132-true-d1247065e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1247053e12-true-d1247094e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247053e12-true-d1247094e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1247053e12-true-d1247094e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1246788e143-true-d1247145e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246788e143-true-d1247145e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1246788e143-true-d1247145e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1247119e58-true-d1247206e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247119e58-true-d1247206e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1247119e58-true-d1247206e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1246660e14-true-d1247283e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246660e14-true-d1247283e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1246660e14-true-d1247283e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1246660e23-true-d1247336e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246660e23-true-d1247336e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1246660e23-true-d1247336e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1246660e27-true-d1247391e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1246660e27-true-d1247391e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1246660e27-true-d1247391e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1247384e10-true-d1247418e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247384e10-true-d1247418e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1247384e10-true-d1247418e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1245375e64-true-d1247472e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1245375e64-true-d1247472e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1245375e64-true-d1247472e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1247449e4-true-d1247505e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247449e4-true-d1247505e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1247449e4-true-d1247505e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.41']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1247525e54-true-d1247537e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1247525e54-true-d1247537e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1247525e54-true-d1247537e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']])]"
         id="d42e24384-true-d1247846e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24384-true-d1247846e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']] (rule-reference: d42e24384-true-d1247846e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40'] | self::hl7:id | self::hl7:code[(@code = '229819007' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1247553e14-true-d1248186e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247553e14-true-d1248186e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40'] | hl7:id | hl7:code[(@code = '229819007' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1247553e14-true-d1248186e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1247553e27-true-d1248228e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247553e27-true-d1248228e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1247553e27-true-d1248228e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1247553e55-true-d1248370e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247553e55-true-d1248370e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1247553e55-true-d1248370e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1248248e43-true-d1248478e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248248e43-true-d1248478e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1248248e43-true-d1248478e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1248248e62-true-d1248537e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248248e62-true-d1248537e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1248248e62-true-d1248537e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1248248e108-true-d1248594e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248248e108-true-d1248594e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1248248e108-true-d1248594e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1248598e92-true-d1248628e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1248598e92-true-d1248628e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1248598e92-true-d1248628e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1248248e131-true-d1248676e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248248e131-true-d1248676e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1248248e131-true-d1248676e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1248248e147-true-d1248721e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248248e147-true-d1248721e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1248248e147-true-d1248721e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1248691e67-true-d1248784e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248691e67-true-d1248784e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1248691e67-true-d1248784e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1247553e58-true-d1248952e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247553e58-true-d1248952e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1247553e58-true-d1248952e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1248829e12-true-d1249084e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248829e12-true-d1249084e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1248829e12-true-d1249084e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1248957e50-true-d1249150e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248957e50-true-d1249150e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1248957e50-true-d1249150e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1248957e120-true-d1249212e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248957e120-true-d1249212e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1248957e120-true-d1249212e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1248957e132-true-d1249234e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248957e132-true-d1249234e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1248957e132-true-d1249234e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1249222e12-true-d1249263e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249222e12-true-d1249263e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1249222e12-true-d1249263e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1248957e143-true-d1249314e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248957e143-true-d1249314e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1248957e143-true-d1249314e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1249288e58-true-d1249375e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249288e58-true-d1249375e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1249288e58-true-d1249375e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1248829e14-true-d1249452e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248829e14-true-d1249452e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1248829e14-true-d1249452e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1248829e23-true-d1249505e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248829e23-true-d1249505e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1248829e23-true-d1249505e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1248829e27-true-d1249560e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1248829e27-true-d1249560e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1248829e27-true-d1249560e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1249553e10-true-d1249587e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249553e10-true-d1249587e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1249553e10-true-d1249587e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1247553e66-true-d1249641e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1247553e66-true-d1249641e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1247553e66-true-d1249641e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1249618e4-true-d1249674e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249618e4-true-d1249674e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1249618e4-true-d1249674e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.40']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1249694e54-true-d1249706e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1249694e54-true-d1249706e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1249694e54-true-d1249706e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']])]"
         id="d42e24401-true-d1250015e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24401-true-d1250015e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']] (rule-reference: d42e24401-true-d1250015e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43'] | self::hl7:id | self::hl7:code[(@code = '68518-0' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1249722e12-true-d1250355e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249722e12-true-d1250355e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43'] | hl7:id | hl7:code[(@code = '68518-0' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1249722e12-true-d1250355e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1249722e27-true-d1250397e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249722e27-true-d1250397e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1249722e27-true-d1250397e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1249722e54-true-d1250539e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249722e54-true-d1250539e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1249722e54-true-d1250539e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1250417e44-true-d1250647e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250417e44-true-d1250647e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1250417e44-true-d1250647e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1250417e63-true-d1250706e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250417e63-true-d1250706e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1250417e63-true-d1250706e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1250417e109-true-d1250763e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250417e109-true-d1250763e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1250417e109-true-d1250763e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1250767e92-true-d1250797e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1250767e92-true-d1250797e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1250767e92-true-d1250797e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1250417e132-true-d1250845e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250417e132-true-d1250845e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1250417e132-true-d1250845e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1250417e148-true-d1250890e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250417e148-true-d1250890e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1250417e148-true-d1250890e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1250860e67-true-d1250953e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250860e67-true-d1250953e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1250860e67-true-d1250953e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1249722e58-true-d1251121e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249722e58-true-d1251121e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1249722e58-true-d1251121e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1250998e12-true-d1251253e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250998e12-true-d1251253e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1250998e12-true-d1251253e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1251126e50-true-d1251319e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251126e50-true-d1251319e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1251126e50-true-d1251319e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1251126e120-true-d1251381e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251126e120-true-d1251381e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1251126e120-true-d1251381e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1251126e132-true-d1251403e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251126e132-true-d1251403e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1251126e132-true-d1251403e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1251391e12-true-d1251432e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251391e12-true-d1251432e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1251391e12-true-d1251432e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1251126e143-true-d1251483e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251126e143-true-d1251483e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1251126e143-true-d1251483e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1251457e58-true-d1251544e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251457e58-true-d1251544e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1251457e58-true-d1251544e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1250998e14-true-d1251621e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250998e14-true-d1251621e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1250998e14-true-d1251621e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1250998e23-true-d1251674e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250998e23-true-d1251674e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1250998e23-true-d1251674e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1250998e27-true-d1251729e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1250998e27-true-d1251729e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1250998e27-true-d1251729e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1251722e10-true-d1251756e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251722e10-true-d1251756e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1251722e10-true-d1251756e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1249722e66-true-d1251810e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1249722e66-true-d1251810e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1249722e66-true-d1251810e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1251787e4-true-d1251843e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251787e4-true-d1251843e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1251787e4-true-d1251843e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.43']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1251863e54-true-d1251875e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1251863e54-true-d1251875e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1251863e54-true-d1251875e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']])]"
         id="d42e24416-true-d1252184e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24416-true-d1252184e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']] (rule-reference: d42e24416-true-d1252184e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42'] | self::hl7:id | self::hl7:code[(@code = '443315005' and @codeSystem = '2.16.840.1.113883.6.96')] | self::hl7:statusCode[@code = 'completed' or @nullFlavor] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d1251891e14-true-d1252524e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251891e14-true-d1252524e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42'] | hl7:id | hl7:code[(@code = '443315005' and @codeSystem = '2.16.840.1.113883.6.96')] | hl7:statusCode[@code = 'completed' or @nullFlavor] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d1251891e14-true-d1252524e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low | self::hl7:high)]"
         id="d1251891e29-true-d1252566e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251891e29-true-d1252566e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low | hl7:high (rule-reference: d1251891e29-true-d1252566e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1251891e45-true-d1252708e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251891e45-true-d1252708e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1251891e45-true-d1252708e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1252586e44-true-d1252816e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1252586e44-true-d1252816e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1252586e44-true-d1252816e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1252586e63-true-d1252875e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1252586e63-true-d1252875e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1252586e63-true-d1252875e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1252586e109-true-d1252932e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1252586e109-true-d1252932e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1252586e109-true-d1252932e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1252936e92-true-d1252966e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1252936e92-true-d1252966e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1252936e92-true-d1252966e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1252586e132-true-d1253014e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1252586e132-true-d1253014e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1252586e132-true-d1253014e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1252586e148-true-d1253059e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1252586e148-true-d1253059e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1252586e148-true-d1253059e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1253029e67-true-d1253122e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253029e67-true-d1253122e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1253029e67-true-d1253122e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1251891e49-true-d1253290e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251891e49-true-d1253290e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1251891e49-true-d1253290e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1253167e12-true-d1253422e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253167e12-true-d1253422e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1253167e12-true-d1253422e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1253295e50-true-d1253488e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253295e50-true-d1253488e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1253295e50-true-d1253488e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1253295e120-true-d1253550e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253295e120-true-d1253550e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1253295e120-true-d1253550e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1253295e132-true-d1253572e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253295e132-true-d1253572e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1253295e132-true-d1253572e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1253560e12-true-d1253601e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253560e12-true-d1253601e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1253560e12-true-d1253601e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1253295e143-true-d1253652e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253295e143-true-d1253652e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1253295e143-true-d1253652e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1253626e58-true-d1253713e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253626e58-true-d1253713e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1253626e58-true-d1253713e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1253167e14-true-d1253790e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253167e14-true-d1253790e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1253167e14-true-d1253790e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1253167e23-true-d1253843e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253167e23-true-d1253843e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1253167e23-true-d1253843e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1253167e27-true-d1253898e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253167e27-true-d1253898e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1253167e27-true-d1253898e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1253891e10-true-d1253925e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253891e10-true-d1253925e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1253891e10-true-d1253925e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d1251891e57-true-d1253979e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1251891e57-true-d1253979e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d1251891e57-true-d1253979e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d1253956e4-true-d1254012e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1253956e4-true-d1254012e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d1253956e4-true-d1254012e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.42']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d1254032e54-true-d1254044e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d1254032e54-true-d1254044e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d1254032e54-true-d1254044e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e24433-true-d1254339e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d42e24433-true-d1254339e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e24433-true-d1254339e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d1254060e8-true-d1254652e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254060e8-true-d1254652e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d1254060e8-true-d1254652e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1254060e59-true-d1254806e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254060e59-true-d1254806e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1254060e59-true-d1254806e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1254684e45-true-d1254914e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254684e45-true-d1254914e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1254684e45-true-d1254914e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1254684e64-true-d1254973e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254684e64-true-d1254973e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1254684e64-true-d1254973e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1254684e110-true-d1255030e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254684e110-true-d1255030e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1254684e110-true-d1255030e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1255034e92-true-d1255064e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1255034e92-true-d1255064e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1255034e92-true-d1255064e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1254684e133-true-d1255112e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254684e133-true-d1255112e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1254684e133-true-d1255112e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1254684e149-true-d1255157e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254684e149-true-d1255157e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1254684e149-true-d1255157e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1255127e67-true-d1255220e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255127e67-true-d1255220e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1255127e67-true-d1255220e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1254060e65-true-d1255388e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1254060e65-true-d1255388e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1254060e65-true-d1255388e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1255265e5-true-d1255520e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255265e5-true-d1255520e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1255265e5-true-d1255520e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1255393e50-true-d1255586e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255393e50-true-d1255586e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1255393e50-true-d1255586e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1255393e120-true-d1255648e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255393e120-true-d1255648e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1255393e120-true-d1255648e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1255393e132-true-d1255670e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255393e132-true-d1255670e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1255393e132-true-d1255670e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1255658e12-true-d1255699e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255658e12-true-d1255699e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1255658e12-true-d1255699e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1255393e143-true-d1255750e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255393e143-true-d1255750e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1255393e143-true-d1255750e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1255724e58-true-d1255811e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255724e58-true-d1255811e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1255724e58-true-d1255811e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1255265e7-true-d1255888e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255265e7-true-d1255888e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1255265e7-true-d1255888e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1255265e16-true-d1255941e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255265e16-true-d1255941e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1255265e16-true-d1255941e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1255265e20-true-d1255996e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255265e20-true-d1255996e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1255265e20-true-d1255996e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.55'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.3.16']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1255989e10-true-d1256023e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.55-2020-10-23T093710.html"
              test="not(.)">(elgagab_section_LebensstilKodiert)/d1255989e10-true-d1256023e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1255989e10-true-d1256023e0)</assert>
   </rule>
</pattern>
