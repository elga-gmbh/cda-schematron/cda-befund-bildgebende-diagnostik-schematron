<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.49
Name: Schwangerschaften - unkodiert
Description: Die Sektion "Schwangerschaften" enthält Informationen über vergangene Schwangerschaften, Geburten und Abortus sowie aktuelle Schwangerschaft und erwarteten Geburtstermin.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559-closed">
   <title>Schwangerschaften - unkodiert</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']])]"
         id="d42e22015-true-d1099918e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d42e22015-true-d1099918e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']] (rule-reference: d42e22015-true-d1099918e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[(@code = '10162-6' and @codeSystem = '2.16.840.1.113883.6.1')] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]])]"
         id="d42e22073-true-d1100402e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d42e22073-true-d1100402e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49'] | hl7:id[not(@nullFlavor)] | hl7:code[(@code = '10162-6' and @codeSystem = '2.16.840.1.113883.6.1')] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]] (rule-reference: d42e22073-true-d1100402e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e22111-true-d1100554e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d42e22111-true-d1100554e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e22111-true-d1100554e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1100432e46-true-d1100662e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1100432e46-true-d1100662e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1100432e46-true-d1100662e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1100432e65-true-d1100721e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1100432e65-true-d1100721e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1100432e65-true-d1100721e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1100432e111-true-d1100778e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1100432e111-true-d1100778e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1100432e111-true-d1100778e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1100782e92-true-d1100812e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1100782e92-true-d1100812e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1100782e92-true-d1100812e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1100432e134-true-d1100860e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1100432e134-true-d1100860e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1100432e134-true-d1100860e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1100432e150-true-d1100905e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1100432e150-true-d1100905e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1100432e150-true-d1100905e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1100875e67-true-d1100968e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1100875e67-true-d1100968e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1100875e67-true-d1100968e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e22118-true-d1101136e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d42e22118-true-d1101136e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e22118-true-d1101136e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1101013e15-true-d1101268e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101013e15-true-d1101268e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1101013e15-true-d1101268e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1101141e50-true-d1101334e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101141e50-true-d1101334e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1101141e50-true-d1101334e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1101141e120-true-d1101396e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101141e120-true-d1101396e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1101141e120-true-d1101396e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1101141e132-true-d1101418e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101141e132-true-d1101418e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1101141e132-true-d1101418e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1101406e12-true-d1101447e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101406e12-true-d1101447e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1101406e12-true-d1101447e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1101141e143-true-d1101498e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101141e143-true-d1101498e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1101141e143-true-d1101498e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1101472e58-true-d1101559e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101472e58-true-d1101559e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1101472e58-true-d1101559e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1101013e17-true-d1101636e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101013e17-true-d1101636e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1101013e17-true-d1101636e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1101013e26-true-d1101689e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101013e26-true-d1101689e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1101013e26-true-d1101689e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1101013e30-true-d1101744e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101013e30-true-d1101744e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1101013e30-true-d1101744e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1101737e10-true-d1101771e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101737e10-true-d1101771e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1101737e10-true-d1101771e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/*[not(@xsi:nil = 'true')][not(self::hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']])]"
         id="d42e22130-true-d1102081e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d42e22130-true-d1102081e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']] (rule-reference: d42e22130-true-d1102081e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | self::hl7:id[not(@nullFlavor)] | self::hl7:title[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant)]"
         id="d1101802e8-true-d1102394e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101802e8-true-d1102394e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8'] | hl7:id[not(@nullFlavor)] | hl7:title[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:languageCode[@code = doc('include/voc-1.2.40.0.34.10.173-DYNAMIC.xml')//valueSet[1]/conceptList/*/@code] | hl7:author[hl7:assignedAuthor] | hl7:informant (rule-reference: d1101802e8-true-d1102394e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d1101802e59-true-d1102548e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101802e59-true-d1102548e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d1101802e59-true-d1102548e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d1102426e45-true-d1102656e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1102426e45-true-d1102656e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d1102426e45-true-d1102656e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1102426e64-true-d1102715e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1102426e64-true-d1102715e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1102426e64-true-d1102715e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1102426e110-true-d1102772e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1102426e110-true-d1102772e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1102426e110-true-d1102772e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1102776e92-true-d1102806e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d1102776e92-true-d1102806e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1102776e92-true-d1102806e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d1102426e133-true-d1102854e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1102426e133-true-d1102854e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d1102426e133-true-d1102854e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1102426e149-true-d1102899e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1102426e149-true-d1102899e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1102426e149-true-d1102899e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d1102869e67-true-d1102962e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1102869e67-true-d1102962e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d1102869e67-true-d1102962e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d1101802e65-true-d1103130e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1101802e65-true-d1103130e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d1101802e65-true-d1103130e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d1103007e5-true-d1103262e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103007e5-true-d1103262e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d1103007e5-true-d1103262e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1103135e50-true-d1103328e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103135e50-true-d1103328e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1103135e50-true-d1103328e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1103135e120-true-d1103390e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103135e120-true-d1103390e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1103135e120-true-d1103390e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d1103135e132-true-d1103412e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103135e132-true-d1103412e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d1103135e132-true-d1103412e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1103400e12-true-d1103441e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103400e12-true-d1103441e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1103400e12-true-d1103441e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d1103135e143-true-d1103492e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103135e143-true-d1103492e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d1103135e143-true-d1103492e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1103466e58-true-d1103553e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103466e58-true-d1103553e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1103466e58-true-d1103553e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d1103007e7-true-d1103630e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103007e7-true-d1103630e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d1103007e7-true-d1103630e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d1103007e16-true-d1103683e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103007e16-true-d1103683e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d1103007e16-true-d1103683e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d1103007e20-true-d1103738e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103007e20-true-d1103738e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d1103007e20-true-d1103738e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.49']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d1103731e10-true-d1103765e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.2.49-2019-11-07T093559.html"
              test="not(.)">(elgagab_section_SchwangerschaftenUnkodiert)/d1103731e10-true-d1103765e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d1103731e10-true-d1103765e0)</assert>
   </rule>
</pattern>
