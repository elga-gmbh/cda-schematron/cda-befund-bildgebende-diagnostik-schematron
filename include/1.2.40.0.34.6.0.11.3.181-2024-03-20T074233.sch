<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.181
Name: Kodierung des Befundtextes
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233">
   <title>Kodierung des Befundtextes</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]"
         id="d42e41819-false-d1874924e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="string(@classCode) = ('OBS')">(elgabgd_entry_BefundtextKodierung): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="string(@moodCode) = ('EVN')">(elgabgd_entry_BefundtextKodierung): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181']) &gt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181']) &lt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.5.3.2']) &gt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.5.3.2']) &lt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']) &gt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12'] ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']) &lt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:code[(@code = '121071' and @codeSystem = '1.2.840.10008.2.16.4')]) &gt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:code[(@code = '121071' and @codeSystem = '1.2.840.10008.2.16.4')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:code[(@code = '121071' and @codeSystem = '1.2.840.10008.2.16.4')]) &lt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:code[(@code = '121071' and @codeSystem = '1.2.840.10008.2.16.4')] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:value[@xsi:type='PQ'] | hl7:value[@xsi:type='IVL_PQ'] | hl7:value[@xsi:type='INT'] | hl7:value[@xsi:type='IVL_INT'] | hl7:value[@xsi:type='BL'] | hl7:value[@xsi:type='ST'] | hl7:value[@xsi:type='CV'] | hl7:value[@xsi:type='TS'] | hl7:value[@xsi:type='RTO'] | hl7:value[@xsi:type='RTO_QTY_QTY'] | hl7:value[@xsi:type='RTO_PQ_PQ'] | hl7:value[@nullFlavor] | hl7:value[@xsi:type='CD'] | hl7:value[@xsi:type='ED'][not(@nullFlavor)])"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="$elmcount &gt;= 1">(elgabgd_entry_BefundtextKodierung): Auswahl (hl7:value[@xsi:type='PQ']  oder  hl7:value[@xsi:type='IVL_PQ']  oder  hl7:value[@xsi:type='INT']  oder  hl7:value[@xsi:type='IVL_INT']  oder  hl7:value[@xsi:type='BL']  oder  hl7:value[@xsi:type='ST']  oder  hl7:value[@xsi:type='CV']  oder  hl7:value[@xsi:type='TS']  oder  hl7:value[@xsi:type='RTO']  oder  hl7:value[@xsi:type='RTO_QTY_QTY']  oder  hl7:value[@xsi:type='RTO_PQ_PQ']  oder  hl7:value[@nullFlavor]  oder  hl7:value[@xsi:type='CD']  oder  hl7:value[@xsi:type='ED'][not(@nullFlavor)]) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="$elmcount &lt;= 1">(elgabgd_entry_BefundtextKodierung): Auswahl (hl7:value[@xsi:type='PQ']  oder  hl7:value[@xsi:type='IVL_PQ']  oder  hl7:value[@xsi:type='INT']  oder  hl7:value[@xsi:type='IVL_INT']  oder  hl7:value[@xsi:type='BL']  oder  hl7:value[@xsi:type='ST']  oder  hl7:value[@xsi:type='CV']  oder  hl7:value[@xsi:type='TS']  oder  hl7:value[@xsi:type='RTO']  oder  hl7:value[@xsi:type='RTO_QTY_QTY']  oder  hl7:value[@xsi:type='RTO_PQ_PQ']  oder  hl7:value[@nullFlavor]  oder  hl7:value[@xsi:type='CD']  oder  hl7:value[@xsi:type='ED'][not(@nullFlavor)]) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:value[@xsi:type='ED'][not(@nullFlavor)]) &gt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:value[@xsi:type='ED'][not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:value[@xsi:type='ED'][not(@nullFlavor)]) &lt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:value[@xsi:type='ED'][not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181']"
         id="d42e41825-false-d1874992e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.181')">(elgabgd_entry_BefundtextKodierung): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.181' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:templateId[@root = '1.2.40.0.34.11.5.3.2']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:templateId[@root = '1.2.40.0.34.11.5.3.2']"
         id="d42e41830-false-d1875007e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="string(@root) = ('1.2.40.0.34.11.5.3.2')">(elgabgd_entry_BefundtextKodierung): Der Wert von root MUSS '1.2.40.0.34.11.5.3.2' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']"
         id="d42e41835-false-d1875022e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.6.2.12')">(elgabgd_entry_BefundtextKodierung): Der Wert von root MUSS '2.16.840.1.113883.10.20.6.2.12' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:code[(@code = '121071' and @codeSystem = '1.2.840.10008.2.16.4')]
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:code[(@code = '121071' and @codeSystem = '1.2.840.10008.2.16.4')]"
         id="d42e41841-false-d1875037e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="@nullFlavor or (@code='121071' and @codeSystem='1.2.840.10008.2.16.4' and @displayName='Finding' and @codeSystemName='DCM')">(elgabgd_entry_BefundtextKodierung): Der Elementinhalt MUSS einer von 'code '121071' codeSystem '1.2.840.10008.2.16.4' displayName='Finding' codeSystemName='DCM'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='PQ']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='PQ']"
         id="d42e41851-false-d1875051e0">
      <extends rule="PQ"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="not(@value) or matches(@value, '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(elgabgd_entry_BefundtextKodierung): @value ist keine gültige PQ Zahl <value-of select="@value"/>
      </assert>
      <let name="theUnit" value="@unit"/>
      <let name="UCUMtest"
           value="doc('include/voc-UCUM.xml')/*/ucum[@unit=$theUnit]/@message"/>
      <assert role="warning"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="$UCUMtest='OK' or string-length($UCUMtest)=0">(elgabgd_entry_BefundtextKodierung): value/@unit (PQ) MUSS eine gültige UCUM-Einheit sein (<value-of select="$UCUMtest"/>).</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='IVL_PQ']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='IVL_PQ']"
         id="d42e41853-false-d1875064e0">
      <extends rule="IVL_PQ"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_PQ')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="not(hl7:low/@value) or matches(string(hl7:low/@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(elgabgd_entry_BefundtextKodierung): value/low @value ist keine gültige PQ Zahl<value-of select="hl7:low/@value"/>
      </assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="not(hl7:high/@value) or matches(string(hl7:high/@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(elgabgd_entry_BefundtextKodierung): value/high @value ist keine gültige PQ Zahl<value-of select="hl7:high/@value"/>
      </assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="not(hl7:center/@value) or matches(string(hl7:center/@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(elgabgd_entry_BefundtextKodierung): value/center @value ist keine gültige PQ Zahl<value-of select="hl7:center/@value"/>
      </assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='INT']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='INT']"
         id="d42e41855-false-d1875079e0">
      <extends rule="INT"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:INT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="not(@value) or matches(@value, '^-?[1-9]\d*$|^+?\d*$')">(elgabgd_entry_BefundtextKodierung): @value ist keine gültige INT Zahl <value-of select="@value"/>
      </assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='IVL_INT']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='IVL_INT']"
         id="d42e41857-false-d1875090e0">
      <extends rule="IVL_INT"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'IVL_INT')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:IVL_INT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='BL']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='BL']"
         id="d42e41860-false-d1875098e0">
      <extends rule="BL"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BL')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:BL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='ST']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='ST']"
         id="d42e41862-false-d1875106e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='CV']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='CV']"
         id="d42e41864-false-d1875114e0">
      <extends rule="CV"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CV')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CV" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='TS']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='TS']"
         id="d42e41866-false-d1875122e0">
      <extends rule="TS"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="not(*)">(elgabgd_entry_BefundtextKodierung): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='RTO']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='RTO']"
         id="d42e41868-false-d1875133e0">
      <extends rule="RTO"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'RTO')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:RTO" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='RTO_QTY_QTY']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='RTO_QTY_QTY']"
         id="d42e41870-false-d1875141e0">
      <extends rule="RTO_QTY_QTY"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'RTO_QTY_QTY')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:RTO_QTY_QTY" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='RTO_PQ_PQ']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='RTO_PQ_PQ']"
         id="d42e41873-false-d1875149e0">
      <extends rule="RTO_PQ_PQ"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'RTO_PQ_PQ')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:RTO_PQ_PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@nullFlavor]
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='CD']
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='CD']"
         id="d42e41877-false-d1875163e0">
      <extends rule="CD"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='ED'][not(@nullFlavor)]
Item: (elgabgd_entry_BefundtextKodierung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='ED'][not(@nullFlavor)]"
         id="d42e41879-false-d1875171e0">
      <extends rule="ED"/>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(elgabgd_entry_BefundtextKodierung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.181-2024-03-20T074233.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(elgabgd_entry_BefundtextKodierung): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.181
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.181'] and hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]/hl7:value[@xsi:type='ED'][not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]
Item: (elgabgd_entry_BefundtextKodierung)
-->
</pattern>
